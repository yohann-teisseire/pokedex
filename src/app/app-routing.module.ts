import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListPokemonComponent } from "./pages/list-pokemon/list-pokemon.component";
import { DetailPokemonComponent } from "./pages/detail-pokemon/detail-pokemon.component";
import { PageNotFoundComponent } from "./pages/page-not-found/page-not-found.component";
import { EditPokemonFormComponent } from "./pages/edit-pokemon-form/edit-pokemon-form.component";
import { AuthGuard } from "./guards/auth.guard";
import { LoginComponent } from "./pages/login/login.component";
import { AddPokemonFormComponent } from "./pages/add-pokemon-form/add-pokemon-form.component";

const appRoutes: Routes = [
    { path: 'login', component: LoginComponent },
    {
        path: 'pokemon',
        canActivate: [AuthGuard],
        children: [
            { path: 'all', component: ListPokemonComponent },
            { path: 'add', component: AddPokemonFormComponent },
            { path: 'edit/:id', component: EditPokemonFormComponent },
            { path: ':id', component: DetailPokemonComponent },
        ]
    },

    { path: '', redirectTo: 'login', pathMatch: 'full' },
    { path: '**', component: PageNotFoundComponent },
]

@NgModule({
    imports: [
        RouterModule.forRoot(appRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule { }