import { Component, OnInit } from '@angular/core';
import { Pokemon } from './entities/pokemon';
import { POKEMONS } from './mock/mock-pokemon';
import { AuthService } from "./services/auth.service";
import { Router } from "@angular/router";
import { TranslateService } from "@ngx-translate/core";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {


  message: string;
  title = 'Pokemon app';

  constructor(
    private authService: AuthService,
    private router: Router,
    private translate: TranslateService) {
    translate.setDefaultLang('en');
  }

  ngOnInit(): void {

  }

  useLanguage(language: string) {
    this.translate.use(language);
  }

  logout() {
    this.authService.logout();
    this.message = 'Vous êtes déconnecté !!';
    this.router.navigate(['login']);
  }
}
