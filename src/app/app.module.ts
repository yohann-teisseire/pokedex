import { HttpClientModule, HttpClient } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { AppRoutingModule } from "./app-routing.module";



import { AppComponent } from './app.component';
import { BorderCardDirective } from './directives/border-card.directive';
import { PokemonTypeColorPipe } from './pipes/pokemon-type-color.pipe';
import { ListPokemonComponent } from './pages/list-pokemon/list-pokemon.component';
import { DetailPokemonComponent } from './pages/detail-pokemon/detail-pokemon.component';
import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component';
import { PokemonsService } from "./services/pokemons.service";
import { PokemonFormComponent } from './pages/pokemon-form/pokemon-form.component';
import { EditPokemonFormComponent } from './pages/edit-pokemon-form/edit-pokemon-form.component';
import { InMemoryDataService } from "./services/in-memory-data.service";
import { HttpClientInMemoryWebApiModule } from "angular-in-memory-web-api";
import { SearchPokemonComponent } from './pages/search-pokemon/search-pokemon.component';
import { LoaderComponent } from './pages/loader/loader.component';
import { AuthGuard } from "./guards/auth.guard";
import { LoginComponent } from './pages/login/login.component';
import { AuthService } from "./services/auth.service";
import { AddPokemonFormComponent } from './pages/add-pokemon-form/add-pokemon-form.component';

// AoT requires an exported function for factories
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

@NgModule({
  declarations: [
    AppComponent,
    BorderCardDirective,
    PokemonTypeColorPipe,
    ListPokemonComponent,
    DetailPokemonComponent,
    PageNotFoundComponent,
    PokemonFormComponent,
    EditPokemonFormComponent,
    SearchPokemonComponent,
    LoaderComponent,
    LoginComponent,
    AddPokemonFormComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    HttpClientModule,
    HttpClientInMemoryWebApiModule.forRoot(
      InMemoryDataService, { dataEncapsulation: false, passThruUnknownUrl: true }
    ),
    AppRoutingModule,
  ],
  providers: [PokemonsService, InMemoryDataService, AuthGuard, AuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }
