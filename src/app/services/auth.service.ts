import { Injectable } from '@angular/core';
import { Observable } from "rxjs/Observable";
import { of } from 'rxjs/observable/of';
import { tap, delay } from "rxjs/operators";

@Injectable()
export class AuthService {

  isLoggedIn: boolean = false;
  redirectUrl: string;

  login(name: string, password: string): Observable<boolean> {
    let isLoggedIn = (name === 'admin' && password === 'admin');

    return of(true).pipe(
      tap(val => {
        this.isLoggedIn = isLoggedIn
      })
    )
  }

  logout(): void {
    this.isLoggedIn = false;
  }

}
