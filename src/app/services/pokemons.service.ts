import { Injectable } from '@angular/core';
import { Pokemon } from '../entities/pokemon';
import { POKEMONS } from '../mock/mock-pokemon';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { catchError, map, tap } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';

@Injectable()
export class PokemonsService {

  private pokemonsUrl = 'api/pokemons';
  constructor(private http: HttpClient) { }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.log(error);
      console.log(`${operation} failed: ${error.message}`);

      return of(result as T);
    };
  }

  searchPokemon(term: string): Observable<Pokemon[]> {
    if (!term.trim()) {
      return of([]);
    }

    return this.http.get<Pokemon[]>(`${this.pokemonsUrl}/?name=${term}`).pipe(
      catchError(this.handleError<Pokemon[]>('get pokemons by term', []))
    )
  }

  addPokemon(pokemon: Pokemon): Observable<Pokemon> {
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };

    return this.http.post<Pokemon>(this.pokemonsUrl, pokemon, httpOptions).pipe(
      tap((pokemon: Pokemon) => {
        console.log(`added pokemon with id=${pokemon.id}`)
      }),
      catchError(this.handleError<Pokemon>('addPokemon'))
    );
  }

  getPokemons(): Observable<Pokemon[]> {
    return this.http.get<Pokemon[]>(this.pokemonsUrl).pipe(
      catchError(this.handleError('getPokemon', []))
    );
  }
  getPokemon(id: number): Observable<Pokemon> {
    const url = `${this.pokemonsUrl}/${id}`;

    return this.http.get<Pokemon>(url).pipe(
      catchError(this.handleError<Pokemon>(`getPokemon id=${id}`))
    );
  }

  updatePokemon(pokemon: Pokemon): Observable<Pokemon> {
    const url = `${this.pokemonsUrl}/${pokemon.id}`;

    return this.http.put<Pokemon>(url, pokemon).pipe(
      catchError(this.handleError<Pokemon>(`updatePokemon id=${pokemon.id}`))
    )
  }

  deletePokemon(pokemon: Pokemon): Observable<Pokemon> {
    const url = `${this.pokemonsUrl}/${pokemon.id}`;

    return this.http.delete<Pokemon>(url).pipe(
      catchError(this.handleError<Pokemon>(`deletePokemon id=${pokemon.id}`))
    )
  }

  getPokemonTypes(): string[] {
    return ['Plante', 'Feu', 'Eau', 'Insecte', 'Normal', 'Electrik', 'Poison', 'Fée', 'Vol'];
  }

}
