import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, ParamMap } from "@angular/router";
import { Pokemon } from "../../entities/pokemon";
import { PokemonsService } from "../../services/pokemons.service";

@Component({
  selector: 'app-detail-pokemon',
  templateUrl: './detail-pokemon.component.html',
  styleUrls: ['./detail-pokemon.component.css']
})
export class DetailPokemonComponent implements OnInit {

  pokemon: Pokemon;
  constructor(private route: ActivatedRoute, private router: Router, private pokemonService: PokemonsService) { }

  ngOnInit(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.pokemonService.getPokemon(id)
      .subscribe(
      pokemon => this.pokemon = pokemon
      )
  }

  goBack(): void {
    this.router.navigate(['/pokemon/all']);
  }

  goEditPokemon(pokemon: Pokemon): void {
    let link = ['/pokemon/edit', pokemon.id];
    this.router.navigate(link);
  }

  goDeletePokemon(pokemon: Pokemon): void {
    this.pokemonService.deletePokemon(pokemon).subscribe(
      () => this.goBack()
    )
  }


}
