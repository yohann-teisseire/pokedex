import { Component, OnInit } from '@angular/core';
import { Pokemon } from "../../entities/pokemon";
import { Title } from "@angular/platform-browser";

@Component({
  selector: 'app-add-pokemon-form',
  templateUrl: './add-pokemon-form.component.html',
  styleUrls: ['./add-pokemon-form.component.css']
})
export class AddPokemonFormComponent implements OnInit {

  pokemon: Pokemon = null;
  constructor(private titleService: Title) { }

  ngOnInit() {
    this.titleService.setTitle('Ajouter un pokémon');
    this.pokemon = new Pokemon();
  }

}
