import { Component, OnInit, Input } from '@angular/core';
import { Pokemon } from "../../entities/pokemon";
import { Router } from "@angular/router";
import { PokemonsService } from "../../services/pokemons.service";

@Component({
  selector: 'app-pokemon-form',
  templateUrl: './pokemon-form.component.html',
  styleUrls: ['./pokemon-form.component.css']
})
export class PokemonFormComponent implements OnInit {

  @Input() pokemon: Pokemon;
  isAddForm: boolean;
  types: Array<string>;
  constructor(private router: Router, private pokemonService: PokemonsService) { }

  ngOnInit() {
    this.types = this.pokemonService.getPokemonTypes();
    this.isAddForm = this.router.url.includes('add');
    console.log(this.isAddForm);
  }

  hasType(type: string): boolean {
    let index = this.pokemon.types.indexOf(type);
    if (~index) return true;
    return false;
  }
  selectType($event: any, type: string): void {
    let checked = $event.target.checked;
    if (checked) {
      this.pokemon.types.push(type);
    } else {
      let index = this.pokemon.types.indexOf(type);
      console.log(index);
      if (~index) {
        this.pokemon.types.splice(index, 1);
      }
    }
  }

  isTypesValid(type: string): boolean {
    if (this.pokemon.types.length === 1 && this.hasType(type)) {
      return false;
    }
    if (this.pokemon.types.length >= 3 && !this.hasType(type)) {
      return false;
    }
    return true;
  }

  onSubmit(): void {
    if (this.isAddForm) {
      this.pokemonService.addPokemon(this.pokemon)
        .subscribe(pokemon => {
          this.pokemon = pokemon;
          this.goBack()
        });
    } else {
      this.pokemonService.updatePokemon(this.pokemon)
        .subscribe(_ => this.goBack());
    }
  }

  goBack() {
    let link = ['/pokemon', this.pokemon.id];
    this.router.navigate(link);
  }

}
