import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditPokemonFormComponent } from './edit-pokemon-form.component';

describe('EditPokemonFormComponent', () => {
  let component: EditPokemonFormComponent;
  let fixture: ComponentFixture<EditPokemonFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditPokemonFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditPokemonFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
