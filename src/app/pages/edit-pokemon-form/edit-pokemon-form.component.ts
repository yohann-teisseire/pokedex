import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { PokemonsService } from "../../services/pokemons.service";
import { Pokemon } from "../../entities/pokemon";
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-edit-pokemon-form',
  templateUrl: './edit-pokemon-form.component.html',
  styleUrls: ['./edit-pokemon-form.component.css']
})
export class EditPokemonFormComponent implements OnInit {
  pokemon: Pokemon;
  constructor(private route: ActivatedRoute, private pokemonService: PokemonsService) { }

  ngOnInit(): void {
    let id = +this.route.snapshot.paramMap.get('id');

    this.pokemonService.getPokemon(id)
      .subscribe(
      pokemon => this.pokemon = pokemon
      )
  }

}
