import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { Pokemon } from "../../entities/pokemon";
import { PokemonsService } from "../../services/pokemons.service";
import { Title } from "@angular/platform-browser";

@Component({
  selector: 'app-list-pokemon',
  templateUrl: './list-pokemon.component.html',
  styleUrls: ['./list-pokemon.component.css']
})
export class ListPokemonComponent implements OnInit {

  pokemons: Pokemon[];
  constructor(
    private router: Router,
    private pokemonService: PokemonsService,
    private titleService: Title
  ) { }

  ngOnInit(): void {
    this.titleService.setTitle('Liste des pokémons');
    this.pokemonService.getPokemons()
      .subscribe(
      pokemons => this.pokemons = pokemons
      );

  }

  selectPokemon(pokemon: Pokemon): void {
    let link = ['/pokemon', pokemon.id];
    this.router.navigate(link);
  }

}
